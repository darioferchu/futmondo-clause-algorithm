# Futmondo Clause Algorithm

Program to screw your friends on thursday

<h2>Execution:</h2>

 $ python3 entrypoint.py email pass action status fitness diff [positions] 


<h3>
    Teams report
</h3>
<h6>
    Check data of players given the selected filters.

</h6>

![Bids](assets/teams-2024-09-05 01-17-09.jpg)




<h3>
    Bids report
</h3>
<h6>
    Check "hidden" data. Bids that are placed for each of the players of the championship users.
    Either market bids or direct bids are shown.

</h6>

![Bids](assets/bids-2024-09-05-01-03-55.jpg)

<h2> Currently:  </h2>

- Automatization of token acquisition
- Fetching teams info and players in each team
- Filter the players that are currently increasing their value
- Show results in json format 
- Filter by position
- Avoid injured and doubt players
- Beautify results(HTML)
- ML model for predicting the price in x days, taking into account past prices or something(need tuning)

<h2>To be done:</h2>
- Take into account last days for determining if player is suitable
- Filter by team
- Taken into account the pace of rising price, and the value of the clause, days to recover the money
- Create clause metric for scoring of the clause(Clausemeter)
- Automatic clausulation of suitable players depending on parameters or team necessities


- Use ML model to predict prices of players given several attributes of them(XGBoost?), (d+n)-5, d+n+1 window

<h3>ML model</h3>
- Get data from (d+n)-5, d+n+1 for all players for n=3/5 days.
- Generate X matrix with all that data
- Split into train, test
- Train model
- Test model
- Evaluate model
- TUne model
- Use it to predict the values of players

-Add trending values, not absolutes: Diff prices, not absolute prices


ML CHECK

- Check which prices values are being used(Use last 5 prices for training )
- Check normalization of player when predicted(Also use last 5 prices for training)
- Add price deltas
- Add points?