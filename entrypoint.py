import sys
import argparse
from htmlGenerator import HTMLGenerator
from pricePredictor import PricePredictor
from futmondoApiAdapter import FutmondoApiAdapter
from futmondoProcessor import FutmondoProcessor



def trainModel(processor, positions, change, status):
        allPlayersData=processor.runRostersFetching(change,status,positions)
        pricePredictor=PricePredictor()
        X,y=pricePredictor.prepareData(allPlayersData)
        pricePredictor.trainModel(X,y)    
        
    
def generateHtml(processor, positions, change, status,diff):
    data=processor.runProcess(change,status,diff,positions)
    htmlGen=HTMLGenerator(data,change,status,positions,diff)
    htmlGen.save_html()


def generateBidsHtml(processor, positions, change, status):     
    data=processor.runGetBids()
    htmlGen=HTMLGenerator(data,change,status,positions)
    htmlGen.save_bids_html()        


def getPlayer(processor,positions,change,status):
    processor.runGetPlayer("631fb31971558206dba97cbc",None)

    ("631fb31971558206dba97cbc",None)

def trainPredictWClause(processor, positions, change, status,diff):
    allPlayersData=processor.runAllChampionshipPlayersFetching(change,status,positions,diff)
    pricePredictor=PricePredictor()
    X,y=pricePredictor.prepareData(allPlayersData)
    pricePredictor.trainModel(X,y)

    playerFetched=processor.runGetPlayerComplete("644d90d4479a44042784eb9f",None)
    predictedDAys=pricePredictor.getExpectedInvestmentReturn(playerFetched)
    print("num_dias",predictedDAys)

    predicted=pricePredictor.predictPrice(playerFetched)
    print("Predicho: ")
    print(predicted)
    
def trainPredictWBid(processor, positions, change, status,diff):
    allPlayersData=processor.runAllChampionshipPlayersFetching(change,status,positions,diff)
    pricePredictor=PricePredictor()
    X,y=pricePredictor.prepareData(allPlayersData)
    pricePredictor.trainModel(X,y)

    playerFetched=processor.runGetPlayerComplete("644d90d4479a44042784eb9f",None)
    predictedDAys=pricePredictor.getExpectedInvestmentReturnWBid(playerFetched,83555555)
    print("num_dias",predictedDAys)

    predicted=pricePredictor.predictPrice(playerFetched)
    print("Predicho: ")
    print(predicted)        

def trainPredictWExistentModel(processor):
    pricePredictor=PricePredictor()
    pricePredictor.loadExistentModel()
    
    playerFetched=processor.runGetPlayerComplete("644d90d4479a44042784eb9f",None)
    print("JUGADOR")
    print(playerFetched)
    predictedDAys=pricePredictor.getExpectedInvestmentReturnWBid(playerFetched,83555555)
    
    print("num_dias",predictedDAys)

    predicted=pricePredictor.predictPrice(playerFetched)
    print("Predicho: ")
    print(predicted)


def main():
    parser = argparse.ArgumentParser(description="Utility")
    
    # Email and password
    parser.add_argument("email", type=str, help="Your Futmondo account email")
    parser.add_argument("password", type=str, help="Your Futmondo account password")
    
    # Command
    parser.add_argument("command", choices=["train_model", "generate_html", "generate_bids_html",
                                        "train_predict","train_predict_bid","predict_existant"], help="Action to perform")
    
    # Change filter
    parser.add_argument("change", choices=["positive", "negative", "all"], help="Filter by positive, negative, or all changes")
    
    # Player status filter
    parser.add_argument("status", choices=["okey", "notOkey", "all"], help="Filter by player status")
    
    # Player percentage filter
    parser.add_argument("diff",type=int, nargs='?', default=0,help="Filter by clause/price ratio")
    
    # Positions (optional)
    parser.add_argument("positions", nargs="*", default=['portero', 'defensa', 'centrocampista', 'delantero'], help="List of positions")
    
    # Parse arguments
    args = parser.parse_args()
    
    print(f"Arguments parsed: {args}")

    # Adapter and processor setup
    adapter = FutmondoApiAdapter(args.email, args.password)
    adapter.authenticate()
    processor = FutmondoProcessor(adapter)

    # Execute the appropriate command
    if args.command == "train_model":
        trainModel(processor, args.positions, args.change, args.status)
    elif args.command == "generate_html":
        generateHtml(processor, args.positions, args.change, args.status,args.diff)
    elif args.command == "generate_bids_html":
        generateBidsHtml(processor, args.positions, args.change, args.status)
    elif args.command == "train_predict":
        trainPredictWClause(processor, args.positions, args.change, args.status,args.diff)
    elif args.command == "train_predict_bid":
        trainPredictWBid(processor, args.positions, args.change, args.status,args.diff)
    elif args.command == "predict_existant":
        trainPredictWExistentModel(processor)
        
           

if __name__== "__main__":
    main()
    

