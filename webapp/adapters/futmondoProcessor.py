class FutmondoProcessor:
    
    def __init__(self,adapter):
        self.adapter=adapter

    def runProcess(self,positiveChange,onlyOkeyPlayers,diff,positions):
        teams=self.adapter.getTeams()
        teamsWithPlayers=self.adapter.processTeams(teams,positiveChange,onlyOkeyPlayers,diff,positions)
        return teamsWithPlayers

    def runRostersFetching(self,positiveChange,onlyOkeyPlayers,positions):
        teams=self.adapter.getTeams()
        allPlayers=self.adapter.processRosters(teams,positiveChange,onlyOkeyPlayers,positions)
        return allPlayers

    def runAllChampionshipPlayersFetching(self,positiveChange,onlyOkeyPlayers,positions,diff):
        allPlayersRaw=self.adapter.getChampionshipPlayers()
        allPlayers=self.adapter.filterRoster(allPlayersRaw,positiveChange,onlyOkeyPlayers,positions,diff,None)
        return allPlayers

    def runGetPlayer(self,playerID,teamID):
        p=self.adapter.getPlayer(playerID,teamID)
        return p

    def runGetPlayerComplete(self,playerID,teamID):
        p=self.adapter.processPlayerComplete(playerID,teamID)
        return p
    
    def runGetBids(self):
        bids=self.adapter.processBids()
        return bids    
