"""Welcome to Reflex! This file outlines the steps to create a basic app."""

import reflex as rx
from webapp.models.playerInfo import PlayerInfo
from webapp.models.playerBidInfo import PlayerBidInfo
from webapp.models.playerBidInfoIterable import PlayerBidInfoIterable
from webapp.models.futmondoPlayer import FutmondoPlayer
from webapp.models.futmondoPlayerBids import FutmondoPlayerBids
from webapp.models.futmondoPlayerBidsIterable import FutmondoPlayerBidsIterable
from adapters.futmondoAdapter import FutmondoApiAdapter
from adapters.futmondoProcessor import FutmondoProcessor
from rxconfig import config



class State(rx.State):
    """The app state."""
    count: int=0
    user: str ="darioferchu@hotmail.com"
    players: list[PlayerInfo] = []
    
    playersMock:list[PlayerInfo] = [
        PlayerInfo(name="Juan",value=321,clause=321),
        PlayerInfo(name="Juan",value=321,clause=321),
        PlayerInfo(name="Juan",value=321,clause=321),
    ]
    
    buyoutPlayers:list[FutmondoPlayer] = []
    bidInfo:list[FutmondoPlayerBids] = []
    bidInfoIterable:list[FutmondoPlayerBidsIterable] = []
    
    messages={
        "home":"",
        "buyout":"Most suitable players for buyout",
        "bidinfo":"Bids that are taking place right now between the players",
        "forecasting":"Decide wether a player is a good option for speculating",
    }
    
    currentSection="home"
    currentMessage=messages["home"]
    
    userMail:str = None
    userToken:str = None
    userId:str = None
    
    invalid_login=False
    loadingBuyoutPlayers:bool = False
    loadingBidInfo:bool = False
    
    @rx.event
    def handle_login(self,form:dict):
        futmondoAdapter=FutmondoApiAdapter(mail=form["email"],password=form["password"])
        authenticated=futmondoAdapter.authenticate()
        if authenticated:
            self.userMail = futmondoAdapter.mail
            self.userToken = futmondoAdapter.token
            self.userId = futmondoAdapter.userID
            return rx.redirect("home")
        else:
            self.invalid_login=True
            return rx.toast.error(
                f"Error when loggin in",
                position="bottom-right",
            )
    
        
    @rx.event
    def handle_change_section(self,section:str):
        self.currentSection=section
        self.currentMessage=self.messages[section]
        if section == "buyout":
            self.loadingBuyoutPlayers=True
        if section == "bidinfo":
            self.loadingBidInfo=True    
        return rx.redirect(section)
    
    @rx.event
    def executeBuyout(self,playerId:str,playerName:str):
        return rx.toast.error(
                f"Buyout completed!",
                position="bottom-right",
        )
    
    def logout(self):
        self.userToken = None
        self.userMail = None
        self.userId = None
        return rx.redirect("/")

    def check_login(self):
        if not self.logged_in:
            return rx.redirect("/")
        if self.currentSection=="buyout":
            if len(self.buyoutPlayers)==0: 
                self.getBuyoutPlayers()
            self.loadingBuyoutPlayers=False
        if self.currentSection=="bidinfo":
            if len(self.bidInfo)==0:
                self.getBids()
            self.loadingBidInfo=False

    @rx.var
    def logged_in(self) -> bool:
        return self.userToken is not None


    def getBuyoutPlayers(self):
        futmondoAdapter=FutmondoApiAdapter(mail=self.userMail,token=self.userToken,user_id=self.userId)
        processor = FutmondoProcessor(futmondoAdapter)
        self.buyoutPlayers  = processor.runProcess("positive","okey",70,['portero', 'defensa', 'centrocampista', 'delantero'])
        print(self.buyoutPlayers)
    
    def getBids(self):
        futmondoAdapter=FutmondoApiAdapter(mail=self.userMail,token=self.userToken,user_id=self.userId)
        processor = FutmondoProcessor(futmondoAdapter)
        self.bidInfo  = processor.runGetBids()
        
        futmondoPlayers:list[FutmondoPlayerBidsIterable]=[]
        for bp in self.bidInfo:
            players:list[PlayerBidInfoIterable]=[]
            for p in bp['players']:
                if p['directBids']!=None:
                    for bid in p['directBids']:
                        players.append(
                            PlayerBidInfoIterable(
                                    id=p['id'],
                                    name=p['name'],
                                    points=p['points'],
                                    role=p['role'],
                                    team=p['team'],
                                    value=p['value'],
                                    change=p['change'],
                                    status=p['status'],
                                    clause=p['clause'],
                                    suggestedclause=p['suggestedclause'],
                                    percentageDiff=p['percentageDiff'],
                                    rating=p['rating'],
                                    average=p['average'],
                                    homeAverage=p['homeAverage'],
                                    awayAverage=p['awayAverage'],
                                    averageLastFive=p['averageLastFive'],
                                    matches=p['matches'],
                                    inMarket=p['inMarket'],
                                    inMarketPrice=p['inMarketPrice'],
                                    price=bid['price']
                            )
                        )
            futmondoPlayers.append(
                FutmondoPlayerBidsIterable(
                    teamId=bp['teamId'],
                    teamName=bp['teamName'],
                    userScrappedId=bp['userScrappedID'],
                    players=players
                )
            ) 
        self.bidInfoIterable=futmondoPlayers    
        print(self.bidInfo)
        print("------------")        
        print(self.bidInfoIterable)    
        


def showPlayerInTable(player:PlayerInfo):
    return rx.table.row(
        rx.table.cell(player.name),
        rx.table.cell(player.role),
        rx.table.cell(player.team),
        rx.table.cell(player.value),
        rx.table.cell(player.clause),
        rx.table.cell(f"{player.percentageDiff}%"),
        rx.table.cell(rx.button(
                rx.icon("skull"),
                on_click=State.executeBuyout(player.id,player.name)
            ))       
    )
    
def showPlayerBidInTable(player:PlayerBidInfoIterable):
    return rx.table.row(
        rx.table.cell(player.name),
        rx.table.cell(player.role),
        rx.table.cell(player.team),
        rx.table.cell(player.value),
        rx.table.cell(player.clause),
        rx.table.cell(f"{player.percentageDiff}%"),
        rx.table.cell(player.price),
    )    


def showFutmondoBuyoutsByPlayer(futmondoPlayer:FutmondoPlayer):
    return rx.cond(
       futmondoPlayer.players.length() == 0,
       rx.hstack(),
       rx.hstack(
            rx.vstack(
                rx.heading(futmondoPlayer.teamName, size="8"),
                rx.table.root(
                    rx.table.header(
                        rx.table.row(
                            rx.table.column_header_cell("Name"),
                            rx.table.column_header_cell("Role"),
                            rx.table.column_header_cell("Team"),
                            rx.table.column_header_cell("Value"),
                            rx.table.column_header_cell("Clause"),
                            rx.table.column_header_cell("Percentage"),
                            rx.table.column_header_cell(""),
                        ),
                    ),
                    rx.table.body(
                        rx.foreach(futmondoPlayer.players,showPlayerInTable),
                    ),
                    variant="surface",
                    width="70rem",
                ),
                empty_div(),
                spacing="7",
                justify="center",
                min_height="10vh",
            ),
        )  
    )


def showFutmondoBidInfoByPlayer(futmondoPlayer:FutmondoPlayerBids):
    return rx.cond(
       futmondoPlayer.players.length() == 0,
       rx.hstack(),
       rx.hstack(
            rx.vstack(
                rx.heading(futmondoPlayer.teamName, size="8"),
                rx.table.root(
                    rx.table.header(
                        rx.table.row(
                            rx.table.column_header_cell("Name"),
                            rx.table.column_header_cell("Role"),
                            rx.table.column_header_cell("Team"),
                            rx.table.column_header_cell("Value"),
                            rx.table.column_header_cell("Clause"),
                            rx.table.column_header_cell("Percentage"),
                            rx.table.column_header_cell("Bid"),
                        ),
                    ),
                    rx.table.body(
                        rx.foreach(futmondoPlayer.players,showPlayerBidInTable),
                    ),
                    variant="surface",
                    width="70rem",
                ),
                empty_div(),
                spacing="7",
                justify="center",
                min_height="10vh",
            ),
        )  
    )

def showFutmondoBuyouts():
    return rx.foreach(State.buyoutPlayers,showFutmondoBuyoutsByPlayer)
    
def showFutmondoBidInfo():
    return rx.foreach(State.bidInfoIterable,showFutmondoBidInfoByPlayer)    
        
    
def bidsTable():
    return rx.table.root(
        rx.table.header(
            rx.table.row(
                rx.table.column_header_cell("Name"),
                rx.table.column_header_cell("Value"),
                rx.table.column_header_cell("Clause")
            ),
        ),
        rx.table.body(
            rx.foreach(State.playersMock,showPlayerInTable),
        ),
        variant="surface",
        size="3"
    )

def buyoutTable():
    return rx.table.root(
        rx.table.header(
            rx.table.row(
                rx.table.column_header_cell("Name"),
                rx.table.column_header_cell("Value"),
                rx.table.column_header_cell("Clause")
            ),
        ),
        rx.table.body(
            rx.foreach(State.playersMock,showPlayerInTable),
        ),
        variant="surface",
        size="3"
    )


def my_div():
    return rx.el.div(
        rx.el.p("U"),
    )

def empty_div():
    return rx.el.div(
    )
    

def showSpinner():
    return rx.cond(
        State.loadingBuyoutPlayers,
        rx.hstack(
            rx.spinner(size="3"),
            justify="center",
            min_height="10vh"
        )
    )
    
def showBidSpinner():
    return rx.cond(
        State.loadingBidInfo,
        rx.hstack(
            rx.spinner(size="3"),
            justify="center",
            min_height="10vh"
        )
    )        
    
    
def form():
    return rx.form(
        rx.card(
            rx.vstack(
                rx.center(
                    rx.heading(
                        "Sign in to your account",
                        size="6",
                        as_="h2",
                        text_align="center",
                        width="100%",
                    ),
                    direction="column",
                    spacing="5",
                    width="100%",
                ),
                rx.vstack(
                    rx.text(
                        "Email address",
                        size="3",
                        weight="medium",
                        text_align="left",
                        width="100%",
                    ),
                    rx.input(
                        placeholder="user@reflex.dev",
                        type="email",
                        size="3",
                        width="100%",
                        id="email",
                        required=True,
                    ),
                    justify="start",
                    spacing="2",
                    width="100%",
                ),
                rx.vstack(
                    rx.hstack(
                        rx.text(
                            "Password",
                            size="3",
                            weight="medium",
                        ),
                        justify="between",
                        width="100%",
                    ),
                    rx.input(
                        placeholder="Enter your password",
                        type="password",
                        size="3",
                        width="100%",
                        id="password",
                        required=True,
                    ),
                    spacing="2",
                    width="100%",
                ),
                rx.button("Sign in", size="3", width="100%", type="submit"),
                spacing="6",
                width="100%",
            ),
            size="4",
            max_width="28em",
            width="100%",
            justify="center",
        ),
        justify="center",
        on_submit=State.handle_login,  # Vincula el evento correctamente
    )    


# -------------------
# -------------------

def show_title():
    return rx.heading("FUTMONDO MANAGER", size="8",justify="center")



def header():
    return rx.hstack(
            rx.heading("FUTMONDO MANAGER", size="8"),
            rx.link(
                rx.button(
                    rx.icon("user"),
                    State.userMail
                ),
            ),
            rx.button(
                rx.icon("log-out"),
                on_click=State.logout()
            ),
            spacing="9",
            justify="center",
            min_height="10vh",
    )

def subHeader():
    return rx.hstack(
            rx.text("Manager application for enhancing fantasy league experience",
                size="5"
            ),
            justify="center",
            min_height="10vh"
    )
    

def menu():
    return rx.hstack(
        rx.button(
            "Buyout",
            on_click=lambda: State.handle_change_section("buyout"),
        ),
        rx.button(
            "Bid info",
            on_click=lambda: State.handle_change_section("bidinfo")
        ),
        rx.button(
            "Forecasting",
            on_click=lambda: State.handle_change_section("forecasting")
        ),        
        justify="center",
        min_height="10vh"
    )    
    
def submenu():
    return rx.hstack(
        rx.text(State.currentMessage,
            size="5"
        ),
        justify="center",
        min_height="10vh"
    )


# -------------------
# -------------------


    
def index() -> rx.Component:
    return rx.container(
        rx.color_mode.button(position="top-right"),
        header(),
        subHeader(),
        menu(),
        showSpinner()
    )
    
def buyout():
    return rx.container(
        rx.color_mode.button(position="top-right"),
        header(),
        menu(),
        submenu(),
        showSpinner(),
        showFutmondoBuyouts()
    )


def bidinfo():
    return rx.container(
        rx.color_mode.button(position="top-right"),
        header(),
        subHeader(),
        menu(),
        submenu(),
        showBidSpinner(),
        showFutmondoBidInfo()
    )


def forecasting():
    return rx.container(
        rx.color_mode.button(position="top-right"),
        header(),
        subHeader(),
        menu(),
        submenu()
    )    


def login() -> rx.Component:
    return rx.container(
        rx.color_mode.button(position="top-right"),
        rx.hstack(
            min_height="10vh",
        ),
        rx.hstack(
            show_title(),
            justify="center",
            min_height="20vh",
        ),
        rx.hstack(
            rx.vstack(
                empty_div(),
                justify="center",
                min_width="0vh",
            ),
            rx.vstack(
                form(),
                justify="center",
                min_height="20vh",
            ),
            justify="center",
            
        ),
    )






app = rx.App()
app.add_page(login,route="/")
app.add_page(index,route="/home",on_load=State.check_login())
app.add_page(buyout,route="/buyout",on_load=State.check_login())
app.add_page(bidinfo,route="/bidinfo",on_load=State.check_login())
app.add_page(forecasting,route="/forecasting",on_load=State.check_login())
