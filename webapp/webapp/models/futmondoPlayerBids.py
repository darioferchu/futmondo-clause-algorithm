import reflex as rx
from .playerBidInfo import PlayerBidInfo

class FutmondoPlayerBids(rx.Base):
    teamId:str
    teamName:str
    userScrappedId:str
    players: list[PlayerBidInfo]