import reflex as rx
from .playerInfo import PlayerInfo

class FutmondoPlayer(rx.Base):
    teamId:str
    teamName:str
    userScrappedId:str
    players: list[PlayerInfo]
    