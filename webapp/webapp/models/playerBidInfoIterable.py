import reflex as rx
from .bid import Bid
class PlayerBidInfoIterable(rx.Base):
    """Modelo para la representación del jugador"""
    id: str = None
    name: str
    points: float = 0.0
    role: str = None
    team: str = None
    value: int
    change: int = 0
    status: str = None
    clause: int
    suggestedclause: int = 0
    percentageDiff: float = 0.0
    rating: int = 0
    average: float = 0.0
    homeAverage: float = 0.0
    awayAverage: float = 0.0
    averageLastFive: float = 0.0
    matches: int = 0
    inMarket: str = None
    inMarketPrice: str = None
    price:int
    