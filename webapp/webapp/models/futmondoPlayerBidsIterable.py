import reflex as rx
from .playerBidInfoIterable import PlayerBidInfoIterable

class FutmondoPlayerBidsIterable(rx.Base):
    teamId:str
    teamName:str
    userScrappedId:str
    players: list[PlayerBidInfoIterable]