from xgboost import XGBRegressor
import numpy as np
import pandas as pd
from sklearn.preprocessing import MinMaxScaler,LabelEncoder
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error,mean_absolute_percentage_error
import joblib

# Price predictor for prices of players, in order to improve
# decision making when playing

class PricePredictor:
    
    xgboost_model='models/xgboost.pkl'
    
    def __init__(self,model='xgboost',thresholdPercentage=10,selectedFeatures=None,nprices=5):
        self.modelName=model
        self.model=self.initializeModel()
        self.thresholdPercentage=thresholdPercentage
        self.selectedLabel='value'
        self.selectedPrices=[]
        self.standardizedPrices = [f'p_{i}' for i in range(nprices)]
        self.nprices=nprices
        self.labelEncoderTeam=LabelEncoder()
        self.labelEncoderRole=LabelEncoder()
        self.scaler=MinMaxScaler()
        if selectedFeatures is None:
            self.selectedFeatures = ['team', 'role', 'rating', 'average', 'homeAverage', 'awayAverage', 'averageLastFive', 'matches', 'clause', 'suggestedclause']
            print(self.selectedFeatures)
            print("++++++++++++++++++++")
        else:
            self.selectedFeatures=selectedFeatures    

    # Initialize xgBoost model
    def initializeModel(self):
        return XGBRegressor()

    def loadExistentModel(self, model_path=xgboost_model):
        self.model = joblib.load(model_path)
        print(f"Modelo cargado desde '{model_path}'")
    
    # Prepare data for training pipeline
    def prepareData(self,dataDict):
        df=pd.DataFrame(dataDict)
        
        df['team']=self.labelEncoderTeam.fit_transform(df['team'])
        df['role']=self.labelEncoderRole.fit_transform(df['role'])
        
        print("Data before selecting, to check number of price columns")
        print(df)
        print("++++++++++++++++++++")
        
        self.selectedPrices = [f'price{i}' for i in range(len(df.filter(like='price').columns) - self.nprices, len(df.filter(like='price').columns))]

        print("Precios elegidos")
        print(self.selectedPrices)

        self.selectedFeatures=self.selectedFeatures+self.selectedPrices
        selectedFeaturesPlusY=self.selectedFeatures+[self.selectedLabel]
        
        #Remove rows with missing values
        df=df[selectedFeaturesPlusY].dropna()

        print("After drop")
        print(df.columns)
        print("------")


        X=df[self.selectedFeatures]
        y=df[self.selectedLabel]
        print("Before scale")
        print(X.columns)
        print("------")
        X= self.scaler.fit_transform(X)
        return X,y
        
        
    # Train the model
    def trainModel(self,X,y):
        X_train,X_test,y_train,y_test= train_test_split(X,y,test_size=0.2,random_state=42)
        self.model.fit(X_train,y_train)
        y_pred=self.model.predict(X_test)
        mse=mean_squared_error(y_test,y_pred)
        mape = mean_absolute_percentage_error(y_test, y_pred)
        print(mse)
        print(mape)
        joblib.dump(self.model, self.xgboost_model)

    # Evaluate model performance
    def evaluateModel(self, X_test, y_test):
        y_pred = self.model.predict(X_test)
        mse = mean_squared_error(y_test, y_pred)
        mape = mean_absolute_percentage_error(y_test, y_pred)
        print(f"Mean Squared Error: {mse}")
        print(f"Mean Absolute Percentage Error: {mape}")

    #Predict price for a single player
    def predictPrice(self, X_player):
        
        df_player = pd.DataFrame([X_player])
        
        #Encoders
        df_player['team']=self.labelEncoderTeam.transform(df_player['team'])
        df_player['role']=self.labelEncoderRole.transform(df_player['role'])

        # nprices
        price_columns = [f'price{i}' for i in range(len(df_player.filter(like='price').columns) - self.nprices, len(df_player.filter(like='price').columns))]

        # Ensure the selected features include the correct price columns and other features
        X_selected = df_player[self.selectedFeatures]
        
        # Scale to match training
        X_selected = self.scaler.transform(X_selected)

        # Predict the price
        return self.model.predict(X_selected)[0]

    @staticmethod
    def update_prices_dict(player, price_columns, next_price):
        # Remove the oldest price and shift others
        for i in range(len(price_columns) - 1):
            player[price_columns[i]] = player[price_columns[i + 1]]
        
        # Add the new predicted price to the last column
        player[price_columns[-1]] = next_price
        return player


    #Predict how many days to recover investment
    def getExpectedInvestmentReturn(self, X_player):
        # Depending on the clause of the player, calculate how many days
        # needed to recover the investment paid if the player is sold.
        finalAmount=X_player['clause']
        nDays=0
        nextPrice=X_player['price5']
        limitDays=21
        
        player=X_player.copy()
        priceColumns = self.selectedPrices
        
        print("Arranco el bucle")
        print("Clausula: ",finalAmount)
        print("Precio: ",nextPrice)

        # player=self.update_prices_dict(player,priceColumns,nextPrice)

        while nextPrice<finalAmount and nDays<limitDays:
            print("Antes ",player['price1'],player['price2'],player['price3'],player['price4'],player['price5'])
            prevPrice=nextPrice
            nextPrice=self.predictPrice(player)    
            nDays+=1
            print("Clausula: "+str(finalAmount)+" - Nuevo Precio: " +str(nextPrice)+" - Subida: " +str(nextPrice-prevPrice))
            if nextPrice>=finalAmount:
                print("HECHO!")
                return nDays
            else: 
                #Update X_player, adding new next price and removing the most antique price
                player=self.update_prices_dict(player,priceColumns,nextPrice)
                print("Después ",player['price1'],player['price2'],player['price3'],player['price4'],player['price5'])
                print("------------------")

        
        return nDays
    
    
    #Predict how many days to recover investment
    def getExpectedInvestmentReturnWBid(self, X_player,bid):
        # Depending on the clause of the player, calculate how many days
        # needed to recover the investment paid if the player is sold.
        finalAmount=bid
        nDays=0
        nextPrice=X_player['price5']
        limitDays=21
        
        player=X_player.copy()
        priceColumns = self.selectedPrices
        
        print("Arranco el bucle")
        print("Pagado: ",finalAmount)
        print("Precio: ",nextPrice)

        # player=self.update_prices_dict(player,priceColumns,nextPrice)

        while nextPrice<finalAmount and nDays<limitDays:
            print("Antes ",player['price1'],player['price2'],player['price3'],player['price4'],player['price5'])
            prevPrice=nextPrice
            nextPrice=self.predictPrice(player)    
            nDays+=1
            print("Clausula: "+str(finalAmount)+" - Nuevo Precio: " +str(nextPrice)+" - Subida: " +str(nextPrice-prevPrice))
            if nextPrice>=finalAmount:
                print("HECHO!")
                return nDays
            else: 
                #Update X_player, adding new next price and removing the most antique price
                player=self.update_prices_dict(player,priceColumns,nextPrice)
                print("Después ",player['price1'],player['price2'],player['price3'],player['price4'],player['price5'])
                print("------------------")

        
        return nDays

