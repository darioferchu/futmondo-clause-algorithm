class HTMLGenerator:
    headers = [
        "Player ID", "Name", "Points", "Role", "Team", "Value", 
        "Change", "Clause", "Suggested Clause","Diff(%)", "Status", 
        "Rating", "Average", "Home Average", "Away Average", 
        "Average Last Five", "Matches"
    ]
    
    filteredHeaders = [
        "Name", "Points", "Role", "Team", "Value", 
        "Change", "Clause", "Suggested Clause","Diff(%)", 
         "Average", "Home Average", "Away Average", 
        "Average Last Five", "Matches"
    ]
    
    filteredBidHeaders = [
        "Name", "Points", "Role", "Team", "Value", 
        "Change", "Clause", "Suggested Clause","Diff(%)", 
         "Average", "Home Average", "Away Average", 
        "Average Last Five", "Matches","bids","directBids"
    ]
    
    fields=[
        'name', 'points', 'role', 'team', 
        'value','change','clause','suggestedclause','percentageDiff', 
        'average', 'homeAverage', 
        'awayAverage', 'averageLastFive', 'matches'        
    ]
    
    filteredFields=[
        'name', 'points', 'role', 'team', 
        'value','change','clause','suggestedclause','percentageDiff', 
        'average', 'homeAverage', 
        'awayAverage', 'averageLastFive', 'matches'        
    ]
    
    pathPlayers="html_results/teams_players.html"
    pathBids="html_results/teams_bids.html"
    
    def __init__(self, data, positiveChange, onlyOkeyPlayers, positions,clausepriceratio=None):
        self.data = data
        self.positiveChange = positiveChange
        self.onlyOkeyPlayers = onlyOkeyPlayers
        self.positions = positions
        self.clausepriceratio=clausepriceratio
    
    def format_price(self, value):
        """Formats a number to Spanish format (1.000,11)"""
        return f"{value:,.2f}".replace(",", "X").replace(".", ",").replace("X", ".")
   
    def createPlottablePlayerDataArray(self,player):
        playerData = [
            player['name'], player['points'], player['role'], player['team'], 
            self.format_price(player['value']),self.format_price(player['change']),self.format_price(player['clause']),self.format_price(player['suggestedclause']),player['percentageDiff'], 
            player['average'], player['homeAverage'], 
            player['awayAverage'], player['averageLastFive'], player['matches']
        ]
        return playerData
        
    def createBidPlottablePlayerDataArray(self,player):
        playerData = [
            player['name'], player['points'], player['role'], player['team'], 
            self.format_price(player['value']),self.format_price(player['change']),self.format_price(player['clause']),self.format_price(player['suggestedclause']),player['percentageDiff'], 
            player['average'], player['homeAverage'], 
            player['awayAverage'], player['averageLastFive'], player['matches'],player['bids'],player['directBids']
        ]
        return playerData            
             
    def generate_html(self):
        # Construct the title based on parameters
        title = "Teams and Players Report"
        
        # Generate a readable string for the positions list
        positions_str = ", ".join(self.positions) if self.positions else "All Positions"
        
        # Construct the subtitle based on parameters
        subtitle = (
            f"Positive Change: {self.positiveChange}<br>"
            f"Only Okey Players: {self.onlyOkeyPlayers}<br>"
            f"Positions: {positions_str}<br>"
            f"Clause/Price Ratio: {self.clausepriceratio}"
        )
        
        html = f"""
        <html>
        <head>
            <title>{title}</title>
            <style>
                table {{width: 100%%; border-collapse: collapse;}}
                th, td {{border: 1px solid black; padding: 10px; text-align: left;}}
                th {{background-color: #f2f2f2;}}
                h2 {{color: #2c3e50;}}
                .subtitle {{font-size: 14px; color: #555;}}
            </style>
        </head>
        <body>
        <h1>{title}</h1>
        <div class="subtitle">
            {subtitle}
        </div>
        """

        for team in self.data:
            html += f"<h2>{team['teamName']} (Team ID: {team['teamId']})</h2>"
            if team['players']:
                # Get a list of price columns from the first player
                price_columns = sorted([key for key in team['players'][0].keys() if key.startswith('price')])

                # Generate table headers including dynamic price columns
                html += "<table><tr>"
                for header in self.filteredHeaders:
                    html += f"<th>{header}</th>"
                for price_col in price_columns:
                    html += f"<th>{price_col.capitalize()}</th>"
                html += "</tr>"

                # Generate table rows for each player
                for player in team['players']:
                    html += "<tr>"
                    row_data = self.createPlottablePlayerDataArray(player)
                    for data in row_data:
                        html += f"<td>{data}</td>"
                    for price_col in price_columns:
                        html += f"<td>{player.get(price_col, 'N/A')}</td>"  # Use .get() to safely access the price fields
                    html += "</tr>"
                html += "</table><br>"
            else:
                html += "<p>No players found for this team.</p>"
        
        html += """
        </body>
        </html>
        """
        return html
    
    def generate_bids_html(self):
        # Construct the title based on parameters
        title = "Bids Report"
        
        html = f"""
        <html>
        <head>
            <title>{title}</title>
            <style>
                table {{width: 100%; border-collapse: collapse;}}
                th, td {{border: 1px solid black; padding: 10px; text-align: left;}}
                th {{background-color: #f2f2f2;}}
                h2 {{color: #2c3e50;}}
                .subtitle {{font-size: 14px; color: #555;}}
                .bid-cell {{background-color: #d4edda;}}  /* Light green background for bids */
            </style>
        </head>
        <body>
        <h1>{title}</h1>
        """

        for team in self.data:
            html += f"<h2>{team['teamName']} (Team ID: {team['teamId']})</h2>"
            if team['players']:
                # Generate table headers including dynamic price and bid columns
                html += "<table><tr>"
                for header in self.filteredBidHeaders:
                    html += f"<th>{header}</th>"
                html += "</tr>"

                # Generate table rows for each player
                for player in team['players']:
                    html += "<tr>"
                    row_data = self.createBidPlottablePlayerDataArray(player)
                    for data in row_data:
                        html += f"<td>{data}</td>"
                    html += "</tr>"
                html += "</table><br>"
            else:
                html += "<p>No players found for this team.</p>"
        
        html += """
        </body>
        </html>
        """
        return html

    def save_html(self, filename=pathPlayers):
        html_content = self.generate_html()
        with open(filename, "w") as file:
            file.write(html_content)
        print(f"HTML file generated: {filename}")
        
    def save_bids_html(self, filename=pathBids):
        html_content = self.generate_bids_html()
        with open(filename, "w") as file:
            file.write(html_content)
        print(f"HTML file generated: {filename}")    
        
        