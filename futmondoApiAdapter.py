import requests

class FutmondoApiAdapter:
    URL_LOGIN= "https://api.futmondo.com/5/login/with_mail"

    URL_ROSTER="https://api.futmondo.com/1/userteam/roster"

    URL_PLAYER="https://api.futmondo.com/1/player/summary"

    URL_CHAMPIONSHIP="https://api.futmondo.com/2/championship/teams"

    URL_CHAMPIONSHIPPLAYERS="https://api.futmondo.com/5/league/championshipplayers"

    CHAMPIONSHIP_ID="64dcd510e1853a06072a7145"

    FUTMONDO_TEAMID="504e581e4d8bec9a670000ce"

    discarded_status=['injured','doubt']

    all_positions=['portero','defensa','centrocampista','delantero']

    all_teams=['Alavés', 'Athletic de Bilbao', 'Atlético de Madrid', 'Barcelona', 
    'Betis', 'Celta de Vigo', 'Espanyol', 'Getafe', 'Girona', 'Las Palmas', 
    'Leganés', 'Mallorca', 'Osasuna', 'Rayo Vallecano', 'Real Madrid', 
    'Real Sociedad', 'Real Valladolid', 'Sevilla', 'Valencia', 'Villarreal']
    
    CLOSE_PERCENTAGE=60
    
    def __init__(self,mail,password):
        self.mail=mail
        self.password=password
        self.token=None
        self.userID=None

    def gen_payload_login(self):
        payload_login = {
        "header": {
            "token": "null",
            "userid": ""
        },
        "query": {
            "mail": self.mail,
            "pwd": self.password
        },
        "answer": {}
        }    
        return payload_login


    def gen_payload_teams(self):
        payload_teams={
        "header": {
            "token": self.token,
            "userid": self.userID
        },
        "query": {
            "championshipId": self.CHAMPIONSHIP_ID
        },
        "answer": {}
        }    
        return payload_teams

    def gen_payload_championship_players(self):
        payload_championship_players={
        "header": {
            "token": self.token,
            "userid": self.userID
        },
        "query": {
            "championshipId": self.CHAMPIONSHIP_ID
        },
        "answer": {}
        }    
        return payload_championship_players        


    def gen_payload_roster(self,teamID):
        payload_roster={
        "header": {
            "token": self.token,
            "userid": self.userID
        },
        "query": {
            "championshipId": self.CHAMPIONSHIP_ID,
            "userteamId": teamID
        },
        "answer": {}
        }    
        return payload_roster

    def gen_payload_player(self,teamID,playerID):
        payload_player={
        "header": {
            "token": self.token,
            "userid": self.userID
        },
        "query": {
            "championshipId": self.CHAMPIONSHIP_ID,
            "userteamId": teamID,
            "playerId": playerID
        },
        "answer": {}
        }    
        return payload_player             



    def gen_headers(self):
        headers = {
            "Accept": "*/*",
            "Accept-Encoding": "gzip, deflate, br",
            "Accept-Language": "es-ES,es;q=0.9",
            "Connection": "keep-alive",
            "Content-Type": "application/json; charset=utf-8",
            "Host": "api.futmondo.com",
            "Origin": "https://app.futmondo.com",
            "Referer": "https://app.futmondo.com/",
            "Sec-Fetch-Dest": "empty",
            "Sec-Fetch-Mode": "cors",
            "Sec-Fetch-Site": "same-site",
            "Sec-GPC": "1",
            "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.5005.61 Safari/537.36"
        }
        return headers

    def authenticate(self):
        r=requests.post(self.URL_LOGIN,json=self.gen_payload_login(),headers=self.gen_headers())    
        if r.status_code==200:
            token=r.json()['answer']['mobile']['token']
            userID=r.json()['answer']['mobile']['userid']
            self.token=token
            self.userID=userID
            return True
        else:
            return False

    def getTeams(self):
        r=requests.post(self.URL_CHAMPIONSHIP,json=self.gen_payload_teams(), headers=self.gen_headers())
        if r.status_code==200:
            teams=r.json()['answer']['teams']
            return teams
        else:
            return None    

    def getChampionshipPlayers(self):
        r=requests.post(self.URL_CHAMPIONSHIPPLAYERS,json=self.gen_payload_championship_players(), headers=self.gen_headers())
        if r.status_code==200:
            championshipTeams=r.json()['answer']['players']
            return championshipTeams
        else:
            return None    

    def getRoster(self,teamID):
        r=requests.post(self.URL_ROSTER,json=self.gen_payload_roster(teamID), headers=self.gen_headers())
        if r.status_code==200:
            roster=r.json()['answer']
            return roster
        else:
            return None

    def getPlayer(self,playerID,teamID=None):
        r=requests.post(self.URL_PLAYER,json=self.gen_payload_player(teamID,playerID), headers=self.gen_headers())
        if r.status_code==200:
            player=r.json()['answer']
            return player
        else:
            return None


    # Filtering functions

    @staticmethod
    def isEqualStatus(player,onlyOkeyPlayers):
        if onlyOkeyPlayers=="all":
            return True
        elif onlyOkeyPlayers=="okey":    
            return player['status'] not in FutmondoApiAdapter.discarded_status
        else:    
            return player['status'] in FutmondoApiAdapter.discarded_status
        
        
            
    @staticmethod
    def isEqualChange(player,positiveChange):
        if positiveChange=="all":
            return True
        elif positiveChange=="positive":    
            return player['change']>0
        else:    
            return player['change']<=0

    @staticmethod
    def isPosition(player,positions):
        if player['role'] in positions:
            return True
        else:
            return False
        
    @staticmethod
    def isCloseToPrice(percentage,clause,price):
        if clause is None or clause == 0:
            return False,None
        per=price/clause*100
        if per>percentage:
            return True,per
        else:
            return False,per
        
    @staticmethod
    def hasMarketBids(player):
        print(player['market'])
        print("++++++++++++")
        if player['market'] != False:
            return True
        else:
            return False
        
    @staticmethod
    def hasDirectBids(player):
        if player['direct'] != False:
            return True
        else:
            return False    
        
    @staticmethod
    def hasMarketOrDirectBids(player):
        if player['market'] != False or player['direct'] != False:
            return True
        else:
            return False    
    
    def filterRoster(self,roster,positiveChange,onlyOkeyPlayers,positions,closeClause,teamID=None):
        filteredRoster=[]
        priceFieldPrefix='price'
        for player in roster:
            playerToAdd=False    
                    
            if self.isEqualChange(player,positiveChange) and self.isEqualStatus(player,onlyOkeyPlayers) and self.isPosition(player,positions): 
                playerToAdd=True
                playerDetail=self.getPlayer(player['id'],teamID=None)    
                priceFields={}
                for i in range(0,len(playerDetail['prices'])-1):
                    fieldName=priceFieldPrefix+str(i)
                    priceFields[fieldName]=playerDetail['prices'][i]['price']
                
                print(player['name'])
                print(player['id'])
                if player['id'] == "5d5ea4ec12a917530bc3e4b0":
                    print(player)
                    print("*****")
                    print(playerDetail)    
                    

            if playerToAdd:
                
                clause=playerDetail['championship']['clause']['price']
                transferred=playerDetail['championship']['clause']['transferred']
                suggestedClause=playerDetail['championship']['clause']['suggestedClause']

                if clause == 0:
                    print("Clausula 0 soy PC")
                    clause=None
                if suggestedClause == 0:
                    print("Suggested Clausula 0")
                    suggestedClause=None        
                
                isClose,perClose=self.isCloseToPrice(closeClause,clause,player['value'])
                
                p={
                    "id":player['id'],
                    "name":player['name'],
                    "points":player['points'],
                    "role":player['role'],
                    "team":playerDetail['data']['team'],
                    "value":player['value'],
                    "change":player['change'],
                    "status":player['status'],
                    "clause": clause,
                    "suggestedclause": suggestedClause,
                    "percentageDiff":perClose,
                    "rating":player['rating'],
                    "average":player['average']['average'],
                    "homeAverage":player['average']['homeAverage'],
                    "awayAverage":player['average']['awayAverage'],
                    "averageLastFive":player['average']['averageLastFive'],
                    "matches":player['average']['matches']
                }
                p.update(priceFields)
                
                if isClose and not transferred:
                    filteredRoster.append(p)
            
        return filteredRoster
    
    
    def filterBids(self,roster):
        filteredBids=[]
        for player in roster:
            playerToAdd=False    
                    
            if self.hasMarketBids(player) or self.hasDirectBids(player): 
                playerToAdd=True
                playerDetail=self.getPlayer(player['id'],teamID=None)    
                
                print(player['name'])
                print(player['id'])
                if player['id'] == "52024439cd25bfb837000027":
                    print(player)
                    print("*****")
                    print(playerDetail)    
                    
            if playerToAdd:
                clause=playerDetail['championship']['clause']['price']
                suggestedClause=playerDetail['championship']['clause']['suggestedClause']

                if clause == 0:
                    clause=None
                if suggestedClause == 0:
                    suggestedClause=None        
                
                isClose,perClose=self.isCloseToPrice(self.CLOSE_PERCENTAGE,clause,player['value'])
                
                playerInMarket=None
                playerInMarketPrice=None
                playerBids=None
                playerDirectBids=None
                
                if self.hasMarketBids(player):
                    playerInMarket=player['market']['inMarket']
                    playerInMarketPrice=player['market']['price']
                    playerBids=player['market']['bids']        
                if self.hasDirectBids(player):
                    playerDirectBids=player['direct']
                
                b={
                    "id":player['id'],
                    "name":player['name'],
                    "points":player['points'],
                    "role":player['role'],
                    "team":playerDetail['data']['team'],
                    "value":player['value'],
                    "change":player['change'],
                    "status":player['status'],
                    "clause": clause,
                    "suggestedclause": suggestedClause,
                    "percentageDiff":perClose,
                    "rating":player['rating'],
                    "average":player['average']['average'],
                    "homeAverage":player['average']['homeAverage'],
                    "awayAverage":player['average']['awayAverage'],
                    "averageLastFive":player['average']['averageLastFive'],
                    "matches":player['average']['matches'],
                    "inMarket":playerInMarket,
                    "inMarketPrice":playerInMarketPrice,
                    "bids":playerBids,
                    "directBids":playerDirectBids
                }
                filteredBids.append(b)
            
        return filteredBids

    def filterPlayer(self,player):
        priceFieldPrefix='price'
        priceFields={}
        for i in range(0,len(player['prices'])-1):
            fieldName=priceFieldPrefix+str(i)
            priceFields[fieldName]=player['prices'][i]['price']
            
            clause=player['championship']['clause']['price']
            suggestedClause=player['championship']['clause']['suggestedClause']

            if clause == 0:
                clause=None
            if suggestedClause == 0:
                suggestedClause=None        
                
            p={
                "id":player['data']['id'],
                "name":player['data']['name'],
                "points":player['data']['points'],
                "role":player['data']['role'],
                "team":player['data']['team'],
                "value":player['data']['value'],
                "change":player['data']['change'],
                "status":player['data']['status'],
                "clause": clause,
                "suggestedclause": suggestedClause,
                "rating":player['data']['rating'],
                "average":player['data']['average']['average'],
                "homeAverage":player['data']['average']['homeAverage'],
                "awayAverage":player['data']['average']['awayAverage'],
                "averageLastFive":player['data']['average']['averageLastFive'],
                "matches":player['data']['average']['matches']
            }
            p.update(priceFields)
        return p        


    def processTeams(self,teams,positiveChange,onlyOkeyPlayers,diff,positions):
        teamWithPlayers=[]
        for t in teams:
            teamID=t['id']
            teamName=t['teamname']
            userScrappedID=t['userid']
            
            r=self.getRoster(teamID)
            filteredR=self.filterRoster(r,positiveChange,onlyOkeyPlayers,positions,diff,teamID)

            tInfo={
                "teamId":teamID,
                "teamName":teamName,
                "userScrappedID":userScrappedID,
                "players":filteredR
            }
            teamWithPlayers.append(tInfo)
        return teamWithPlayers    
            
    def processRosters(self,teams,positiveChange,onlyOkeyPlayers,positions,diff=0):
        allPlayers=[]
        for t in teams:
            teamID=t['id']
            teamName=t['teamname']
            userScrappedID=t['userid']
            
            r=self.getRoster(teamID)
            filteredR=self.filterRoster(r,positiveChange,onlyOkeyPlayers,positions,diff,teamID)
            allPlayers=allPlayers+filteredR
        return allPlayers

    def processPlayerComplete(self,playerID,teamID):
        p=self.getPlayer(playerID,teamID)
        p=self.filterPlayer(p)
        return p
    
    
    def processBids(self):
        allBids=[]
        teams=self.getTeams()
        for t in teams:
            teamID=t['id']
            teamName=t['teamname']
            userScrappedID=t['userid']
            r=self.getRoster(teamID)
            teamBids=self.filterBids(r)
            
            tInfo={
                "teamId":teamID,
                "teamName":teamName,
                "userScrappedID":userScrappedID,
                "players":teamBids
            }
            allBids.append(tInfo)
        return allBids
